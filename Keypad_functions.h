/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_DRIVERS_KEYPAD_DRIVER_KEYPAD_FUNCTIONS_H_
#define HAL_DRIVERS_KEYPAD_DRIVER_KEYPAD_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "../../MCAL_drivers/DIO_driver/DIO_functions.h"

#define KEYPAD_ROWS_COUNT_ 4 // number of rows
#define KEYPAD_COLS_COUNT_ 4 // number of columns

typedef struct
{
	DIO_port_t portRows; // rows port
	DIO_port_t portCols; // columns port

	// rows (output)
	u8 pinsR[KEYPAD_ROWS_COUNT_];

	// columns (input)
	u8 pinsC[KEYPAD_COLS_COUNT_];
} Keypad_obj_t;

typedef enum
{
    Keypad_mode_trap,
    Keypad_mode_one_time
} Keypad_mode_t;

typedef const Keypad_obj_t* Keypad_cptr_t;

void Keypad_vidInit(Keypad_cptr_t structPtrKeypadCpy);

u8 Keypad_u8GetPressedKey(Keypad_mode_t enumKeypadModeCpy, Keypad_cptr_t structPtrKeypadCpy);


#endif /* HAL_DRIVERS_KEYPAD_DRIVER_KEYPAD_FUNCTIONS_H_ */

