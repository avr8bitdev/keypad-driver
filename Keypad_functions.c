/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Keypad_functions.h"


// --- internal --- //
static const u8 Keypad_keysMap[KEYPAD_ROWS_COUNT_][KEYPAD_COLS_COUNT_] = {
		{'7', '8', '9', '+'}, // row 0, column 0
		{'4', '5', '6', '-'},
		{'1', '2', '3', '*'},
		{'=', '0', '.', '/'}
};

static u8 Keypad_u8GetPressedKey_(Keypad_cptr_t structPtrKeypadCpy)
{
    for (u8 r = 0; r < KEYPAD_ROWS_COUNT_; r++) // foreach row
    {
        DIO_vidSet_pinValue(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[r], OFF); // turn off current row

        for (u8 c = 0; c < KEYPAD_COLS_COUNT_; c++) // foreach col
        {
            if (DIO_u8Get_pinValue(structPtrKeypadCpy->portCols, structPtrKeypadCpy->pinsC[c]) == 0) // if key pressed
            {
                DIO_vidSet_pinValue(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[r], ON); // turn back on current row

                return Keypad_keysMap[r][c];
            }
        }

        DIO_vidSet_pinValue(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[r], ON); // turn back on current row
    }

    return 0; // nothing pressed
}
// ---------------- //

void Keypad_vidInit(Keypad_cptr_t structPtrKeypadCpy)
{
	for (u8 i = 0; i < KEYPAD_COLS_COUNT_; i++)
	{
		DIO_vidSet_pinDirection(structPtrKeypadCpy->portCols, structPtrKeypadCpy->pinsC[i], INPUT); // col

		DIO_vidActivate_pinPullUp(structPtrKeypadCpy->portCols, structPtrKeypadCpy->pinsC[i]);
	}

	for (u8 i = 0; i < KEYPAD_ROWS_COUNT_; i++)
	{
		DIO_vidSet_pinDirection(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[i], OUTPUT); // row

		DIO_vidSet_pinValue(structPtrKeypadCpy->portRows, structPtrKeypadCpy->pinsR[i], ON);
	}

}

u8 Keypad_u8GetPressedKey(Keypad_mode_t enumKeypadModeCpy, Keypad_cptr_t structPtrKeypadCpy)
{
    u8 key = 0;

	switch (enumKeypadModeCpy)
	{
	    case Keypad_mode_one_time:
	        key = Keypad_u8GetPressedKey_(structPtrKeypadCpy);
	    break;

        case Keypad_mode_trap:
            do
            {
                key = Keypad_u8GetPressedKey_(structPtrKeypadCpy);
            }
            while (!key); // while no key pressed
        break;
	}

	return key;
}

